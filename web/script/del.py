#!/usr/bin/python3
#
# del.py -- Delete the packages that have dependencies in the deploy-packages.json file
#

import json

# Read the JSON file
# Exclude packages that cannot support dependencies
with open('data/deploy-packages.json', 'r', encoding='utf-8') as file:
    data = json.load(file)

# Ensure data contains the 'packages' field
if 'packages' not in data or not isinstance(data['packages'], list):
    raise ValueError("Incorrect JSON format, should contain 'packages' field with a list value")

packages = data['packages']

# Process data
filtered_packages = []
for a in packages:
    dependencies = a.get('dependencies', '').split(',')
    found = False
    for b in packages:
        if a == b:
            continue
        info = b.get('info', '')
        if any(dep in info for dep in dependencies):
            found = True
            break
    if found:
        filtered_packages.append(a)

# Update the 'packages' field
data['packages'] = filtered_packages

# Write back to the JSON file
with open('data/deploy-packages.json', 'w', encoding='utf-8') as file:
    json.dump(data, file, ensure_ascii=False, indent=4)