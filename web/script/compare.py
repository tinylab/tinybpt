#!/usr/bin/python3
#
# compare.py -- Compare the versions of the packages in deploy-pkgs-version.txt with the packages.json file
#

import json
import re

# Read the deploy-pkgs-version.txt file
with open('web/data/deploy-pkgs-version.txt', 'r') as f:
    deploy_versions = [line.strip().replace('.tar.gz', '') for line in f]

# Read the packages.json file
with open('data/packages.json', 'r') as f:
    packages_data = json.load(f)

# Fuzzy match version information and replace
updated_packages = []
for package in packages_data['packages']:
    package_version = package['version']
    for deploy_version in deploy_versions:
        # Use regular expressions for fuzzy matching
        if re.search(deploy_version, package_version):
            package['version'] = deploy_version
            updated_packages.append(package)
            break

# Write the updated data to the deploy-packages.json file
with open('data/deploy-packages.json', 'w') as f:
    json.dump({'packages': updated_packages}, f, indent=4)

print("Update complete, matched items have been written to data/deploy-packages.json.")