#!/usr/bin/python3
#
# ex.py -- Extract the package names from the tinybpt_db.json file
#

import json


def name():
    with open('tinybpt_db.json', 'r', encoding='utf-8') as file:
        data = json.load(file)

    names = [package['name'] for package in data['packages']]

    with open('web/data/full-pkgs-name.txt', 'w', encoding='utf-8') as file:
        for name in names:
            file.write(name + '\n')

def version():
    with open('tinybpt_db.json', 'r', encoding='utf-8') as file:
        data = json.load(file)

    versions = [package['version'] for package in data['packages']]

    with open('web/data/full-pkgs-version.txt', 'w', encoding='utf-8') as file:
        for version in versions:
            file.write(version + '\n')

if __name__ == '__main__':
    version()