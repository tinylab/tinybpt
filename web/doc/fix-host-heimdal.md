
# Buildroot 无法编译 host-heimdal

## 问题描述

checking for /usr/bin/gcc  options needed to detect all undeclared functions... cannot detect
configure: error: in '/home/sword/buildroot-2024.02.6/output/build/host-heimdal-f4faaeaba371fff3f8d1bc14389f5e6d70ca8e17':
configure: error: cannot make /usr/bin/gcc  report undeclared builtins
See 'config.log' for more details
make: *** [package/pkg-generic.mk:273: /home/sword/buildroot-2024.02.6/output/build/host-heimdal-f4faaeaba371fff3f8d1bc14389f5e6d70ca8e17/.stamp_configured] Error 1

https://www.soinside.com/question/jLwkUrGPKPYsVPYrMB7egg

## 问题原因

问题是heimdal的配置脚本使用了

```shell
AC_SYS_LARGEFILE
```

宏，然后使用了其内部的

```shell
ac_cv_sys_large_files
ac_cv_sys_file_offset_bits
```

上述变量，这些变量在该宏的最近更新中已被删除。

正确的修复可能是删除上游 heimdal 的配置脚本中对大文件的特殊处理。

## 解决方法

因为该问题不能通过跳过编译解决，因此做以下修改，在 `buildroot-2024.02.6/package/heimdal/heimdal.mk` 文件中，将

```shell
ac_cv_sys_large_files=1 ac_cv_sys_file_offset_bits=64
```

添加到 `HOST_HEIMDAL_CONF_ENV` 变量中，即

```shell
HOST_HEIMDAL_CONF_ENV = ac_cv_prog_COMPILE_ET=no MAKEINFO=true ac_cv_sys_large_files=1 ac_cv_sys_file_offset_bits=64
```

## 引用

https://www.soinside.com/question/jLwkUrGPKPYsVPYrMB7egg