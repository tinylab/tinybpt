#!/bin/bash
#
# test.sh -- Test the tinybpt script
#

TARGET="./tinybpt"

echo "Running: $TARGET"
$TARGET

echo "Running: $TARGET install stress"
$TARGET install stress

echo "Running: $TARGET uninstall stress -f"
$TARGET uninstall stress -f

echo "Running: $TARGET -get-mirror"
$TARGET -get-mirror

echo "Running: $TARGET find stress"
$TARGET find stress