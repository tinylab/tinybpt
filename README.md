
# TinyBPT

## 介绍

TinyBPT (Tiny Buildroot Packaging Tool) 是一个 `buildroot` 的包管理工具，主要处理 `buildroot` 的包依赖关系，提供包的安装、卸载等功能。

## 安装方法

### 直接安装

如果要在 `buildroot` 构建的子系统中安装，则需要在主机下载后通过构建本地 `HTTP` 服务的方式提供给子系统下载。

```shell
wget https://mirrors.lzu.edu.cn/buildroot-pkgs/riscv64/tinybpt/tinybpt-v0.1-rc1.tar.gz
mkdir -p /etc/tinybpt && mkdir -p /etc/ssl/certs
tar -xvf tinybpt-v0.1-rc1.tar.gz -C /
cd tinybpt
```

### 本地 Makefile 编译安装

本地编译需要先编译 `OpenSSL`，详见 `doc/DESIGN.md` 中的说明。

```shell
git clone https://gitee.com/tinylab/tinybpt.git
cd tinybpt
wget https://curl.se/ca/cacert.pem
make
make install
```

### 本地 CMake 编译安装

```shell
git clone https://gitee.com/tinylab/tinybpt.git tinybpt
cmake -S tinybpt
    -B build \
    -G Ninja
ninja -C build
ninja -C tinybpt/build install
```

### 使用环境变量指定依赖和下载路径（可选）

默认情况下，TinyBPT 会将依赖和下载的文件存储在 `/etc/tinybpt` 和 `/var/cache/tinybpt` 目录下，如果需要指定其他路径，可以使用环境变量 `TINYBPT_DB_PATH` 和 `TINYBPT_DOWNLOAD_PATH`。

```shell
export TINYBPT_DB_PATH=<your_path_to>/tinybpt_db.json
export TINYBPT_DOWNLOAD_PATH=<your_path_to_download>
```

## 使用方法

```shell
# tinybpt
Tiny buildroot packaging tool

Usage: tinybpt <command> [options]

Commands:
    install <package_name>          Install package
    uninstall <package_name> [-f]   Uninstall package (use -f to force uninstall)
    list -all/-installed            List all/installed packages
    find <package_name>             Find package

Options:
    -h, -help
           Show help information
    -V, -version
           Show version information
    -set-mirror <url>               Set mirror URL
    -get-mirror                     Show mirror URL
```

## 基本操作

使用 `tinybpt install/uninstall/list/find <package_name>` 进行包的安装、卸载、查找、列出操作。

## 服务端配置

详见 `doc/SERVER.md` 中的说明。

## 其他说明

- 本工具仅支持 riscv64 buildroot 用户使用。
- 本工具使用 CA 证书为 CA certificates extracted from Mozilla。
- 本项目使用了基于 MIT 协议的第三方库 [nlohmann/json][001] 和 [cpp-httplib][002]。

[001]: https://github.com/nlohmann/json
[002]: https://github.com/yhirose/cpp-httplib
