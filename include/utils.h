//
// utils.h -- Contains declarations of utility functions for string processing, time retrieval, package information lookup, etc.
//

#ifndef UTILS_H
#define UTILS_H

#include <string>
#include <iostream>
#include <ctime>
#include <vector>
#include "tinybpt.h"

std::string trim(const std::string &str);
std::string get_current_time();
std::string find_version_by_name(const std::string &tname, const std::string &file_path);
std::string find_package_by_info(const std::string &info, const std::string &file_path);
void print_help();

#endif // UTILS_H