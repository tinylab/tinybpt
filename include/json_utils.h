//
// json_utils.h -- Contains utility function declarations for processing JSON data
//

#ifndef JSON_UTILS_H
#define JSON_UTILS_H

#include <string>
#include <nlohmann/json.hpp>
#include "tinybpt.h"
#include "utils.h"

void list_packages(const std::string &option);
void find_package(const std::string &name, const std::string &key);
void read_package_info(const std::string &json_file, const std::string &key);
void update_local_package_info(const std::string &json_file, const Package &pkg);

Config read_config(const std::string& file_path);
void set_mirror_url(const std::string& new_mirror_url, const std::string& config_file);
void show_mirror_url(const std::string& config_file);
void show_version(const std::string& config_file);

#endif // JSON_UTILS_H