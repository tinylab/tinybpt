//
// tinybpt.h -- Header file for the package management tool, defines Config and Package structs, and declares related functions.
//

#ifndef TINYBPT_H
#define TINYBPT_H

#include <string>
#include <vector>
#include <nlohmann/json.hpp>

struct Config {
    std::string mirror_url;
    std::string target;
    std::string arch;
    std::string version;
    std::string buildroot_version;
    std::string dirs;
};

struct Package {
    std::string name;
    std::string version;
    std::string info;
    std::string download_time;
    std::string dependencies;
};
extern const std::string db_file;
extern std::string download_path;
extern std::vector<Package> packages;
extern std::string mirror_url;
extern Config config;

void read_mirror(const std::string &option);
void save_local_packages(const nlohmann::json &local_packages);
void remove_package(const std::string &name, bool force);
void list_packages(const std::string &list_type);
bool download_package(const std::string &url, const std::string &output_path, const std::string &name);
void download_and_install_package(const std::string &name, const std::string &url);
void install_package(const std::string &name, const std::string &url);
inline void from_json(const nlohmann::json& j, Package& p);
bool create_symlinks(const std::string &output_path);

#endif // TINYBPT_H