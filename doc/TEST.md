# 测试报告

运行 `script/test.sh` 以运行测试用例。

应看到以下输出（版本号应随更新有所变化）：

```
Running: ./tinybpt
Tiny buildroot packaging tool

Usage: tinybpt <command> [options]

Commands:
    install <package_name>          Install package
    uninstall <package_name> [-f]   Uninstall package (use -f to force uninstall)
    list -all/-installed            List all/installed packages
    find <package_name>             Find package

Options:
    -h, -help
           Show help information
    -V, -version
           Show version information
    -set-mirror <url>               Set mirror URL
    -get-mirror                     Show mirror URL
Running: ./tinybpt install stress
Package stress is not installed
Downloading package: stress
Output Path: /var/cache/tinybpt/stress/
Downloading:  [==================================================] 100 %
./
./usr/
./usr/bin/
./usr/bin/stress
./usr/share/
./usr/share/man/
./usr/share/man/man1/
./usr/share/man/man1/stress.1
Installation complete: stress
Running: ./tinybpt uninstall stress -f
Deleted file: /usr/bin/stress
Deleted file: /usr/share/man/man1/stress.1
Removed package directory: /var/cache/tinybpt/stress
Removed package info from localpackages: stress
Running: ./tinybpt -get-mirror
Current Mirror URL: https://mirrors.lzu.edu.cn
Running: ./tinybpt find stress
Searching installed packages:
Package not found: stress
Searching all packages:
Found package: stress (stress-1.0.7)
Info: BR2_PACKAGE_STRESS
Dependencies:
```