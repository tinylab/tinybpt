
# 设计文档

## 软件架构

```
                            +-------------------+       +-------------------+
                            |                   |       |                   |
                            |    main.cpp       |       |    utils.cpp      |
                            |                   |       |                   |
                            |  - Handle CLI args|       |  - String handling|
                            |  - Call pkg funcs |       |  - Time retrieval |
                            |                   |       |  - Pkg info lookup|
                            +---------+---------+       +---------+---------+
                                    |                           |
                    cacert.pem      |                           |
                         |          v                           v
+---------+---------+    |  +---------+---------+       +---------+---------+
|      server       |    |  |                   |       |                   |
|                   |    v  |  tinybpt.cpp      |<------|  json_utils.cpp   |
| - Metadata Service|------>|                   |       |                   |
| - Download Service|       |  - Install pkg    |       |  - Handle JSON    |
| - Package Storage |       |  - Uninstall pkg  |       |  - List pkgs      |
| - Compile Package |       |  - Download pkg   |       |  - Find pkgs      |
|                   |       |  - Manage deps    |       |  - Update pkg info|
|                   |       |                   |       |  - Read config    |
+---------+---------+       +---------+---------+       +---------+---------+
                                    |                           |
                                    v                           v
                            +---------+---------+       +---------+---------+
                            |                   |       |                   |
                            |  Config struct    |       |  Package struct   | <--- tinybpt_db.json
                            |                   |       |                   |
                            |  - Mirror URL     |       |  - Name           |
                            |  - Version info   |       |  - Version        |
                            |  - config.json    |       |  - Info           |
                            |                   |       |  - Download time  |
                            |                   |       |  - Dependencies   |
                            +-------------------+       +-------------------+
```



## 服务器端

### 概述

服务器端负责提供包的存储和分发服务。它包含以下主要功能：
- 存储包文件
- 提供包的下载链接
- 从源更新包的元数据

### 组件

- **包存储**: 存储所有可用的包文件。
- **元数据服务**: 提供包的元数据，包括版本信息、依赖关系等。
- **下载服务**: 提供包的下载链接，供客户端下载。

### 工作流程

1. **上传包**: 开发者将新包上传到服务器。
2. **更新元数据**: 服务器更新包的元数据，包括版本信息和依赖关系。
3. **提供下载链接**: 服务器生成下载链接，供客户端使用。

## 客户端工具

### 概述

客户端负责从服务器下载包并进行安装、卸载等操作。它包含以下主要功能：
- 下载包
- 安装包
- 卸载包
- 管理包的依赖关系

### 组件

- **main.cpp**: 处理命令行参数，调用包管理功能。
- **utils.cpp**: 提供字符串处理、时间获取和包信息查找等实用功能。
- **tinybpt.cpp**: 实现包的安装、卸载、下载和依赖管理功能。
- **json_utils.cpp**: 处理 JSON 格式的包元数据，读取配置文件。
- **tinybpt_db.json**: 存储配置信息，如镜像 URL 和版本信息。
- **cacert.pem**: CA 证书，用于安全连接。

### 依赖解决

在任意版本 `buildroot` 目录下，运行 `script/` 目录下相关脚本解析依赖并通过后续服务端配置对比处理。

### 编译流程

请先配置 `riscv64` 的 `openssl` 依赖，并采用如下编译方式：

```shell
./Configure linux64-riscv64 no-zlib no-rc2 no-idea no-des no-bf no-cast no-md2 no-mdc2 no-dh no-err no-rc5 no-camellia no-seed no-tests  -static  --prefix=/usr/local/openssl/riscv64 --cross-compile-prefix=riscv64-linux-gnu- -flto

make -j$(nproc)
make install_sw
```

然后移动到 `tinybpt` 目录下，执行如下命令：

```shell
make ARCH=riscv64 -j$(nproc)
```

如果这些流程均在主机（非 `riscv` 机器）中完成，则需要将编译好的二进制文件，依赖文件和 `CA` 证书移动到嵌入式设备（`riscv` 机器）中。

如果是以调试为目的，可以使用 `ARCH=x86_64` 选项进行编译，或者使用 `CMake` 。编译完成后，只需要将依赖文件移动到相应位置，或者在环境变量中设置 `TINYBPT_DB_PATH` 。

### 工作流程

- **下载包**: 客户端从服务器下载指定的包文件。
- **安装包**: 客户端将下载的包文件安装到系统中。
- **卸载包**: 客户端从系统中卸载指定的包。
- **管理依赖**: 客户端根据包的依赖关系，自动下载和安装所需的依赖包。