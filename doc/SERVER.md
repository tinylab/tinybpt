
# 服务器端配置

## 在服务器端配置 buildroot 源码

### 使用工具 tsumugu 来更新源

```shell
wget https://github.com/taoky/tsumugu/releases/download/20240902.2/tsumugu
```

配置运行脚本在 `/web/settings/buildroot.sh` 中。

### 提醒事项

软件源码包大小约为 468 GB。

## 二进制包

### 二进制包源

目前，二进制包在 `http://mirrors.lzu.edu.cn/buildroot-pkgs` 已经开始提供。

### 目录结构

```shell
buildroot/                      -- 源码镜像，直接镜像 buildroot 官方所有包的源码库

buildroot-pkgs/                 -- 二进制包发布目录
  riscv64/                      -- 处理器架构，独立的话方便第三方镜像
    main/2024.02.6/             -- main 用于存放所有 buildroot lts 版本的二进制包（动态编译）
    tools/v6.10/                -- tools 用于存放 Linux lts 内核版本下的 tools 二进制包（以静态编译）
    rootfs/2024.02.6/           -- rootfs 用于存放 rootfs 包
    tinybpt/
      tinybpt-v0.1-rc1.tar.gz   -- tinybpt 二进制包
  aarch64/
    main/2024.02.6/
    tools/v6.10/
  x86_64/
    main/2024.02.6/
    tools/v6.10/
```

### 编译选项

如果要自行编译，编译配置在 `/web/settings/.config` 中，使用编译配置替换原有配置，或使用 `make menuconfig` 自行勾选需要编译的包。

```shell
cd buildroot
make -j$(nproc)
```

### 二进制包的发布

完成后，进入 `output/build` 目录，运行 `web/script/install.sh` 脚本，将编译好的二进制包打包到 `/mnt/tinybpt` 目录下。

运行 `web/script/compare.py` 脚本和 `web/script/del.py` 脚本，对比和删除不必要的包，更新依赖文件。

参见 `/web/scripts/install.sh` 等脚本。

注意：二进制包的编译和发布方式会导致某些包无法编译，原因在于目录结构不同等，不成功的包在 `/web/log` 中记录。

### 提醒事项

为了解决编译中的问题，我们需要在编译前进行一些配置，详情请看 `/web/doc` 目录。

确保您知道为何有些包无法编译，请查看 `/web/log/build-error.log` 文件。

二进制包大小约为 12 GB。
