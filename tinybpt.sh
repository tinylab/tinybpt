#!/bin/sh

if [ -f /etc/tinybpt/tinybpt_db.json ]; then
    export TINYBPT_DB_PATH="/etc/tinybpt/tinybpt_db.json"
fi

# According to the Linux specification, online downloads can be placed under `/var/cache`.

if [ -d /var/cache/tinybpd/ ]; then
    export TINYBPT_DOWNLOAD_PATH="/var/cache/tinybpd"
fi

export PATH